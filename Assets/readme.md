﻿# impatient-chess

A Unity project that implements the rules of "Chess 2". Currently incomplete and not really working.


## Developer Documentation

### Project Setup

* Create an empty Unity 2D project called `impatient-chess`, then close the Unity editor.

* Clone the impatient-chess git repository into some other directory.

* For every file and directory in the git checkout, delete any equivalent in the Unity project and copy the file/directory from git.

* Open the project in Unity and create the essential TextMesh Pro resources (in the Window menu).

* Set the external editor to Rider, then double click on the main script to open the project in Rider.

### Coordinate Convention

Positions are stored in a Vector2Int, with x being row (0-7 = '1'-'8') and y being column (0-7 = 'a'-'h').
Note that this is kinda the reverse of how they are displayed: high rows are displayed towards the top of the screen, and high columns toward the right. Sorry.