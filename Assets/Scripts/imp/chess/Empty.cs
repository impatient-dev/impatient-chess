﻿using System.Collections.Generic;
using imp.chess.game;
using UnityEngine;

namespace imp.chess {

	/// A bunch of empty lists.
	public class Empty {
		public static readonly IReadOnlyList<NormalMove> NormalMove = new List<NormalMove>();
		public static readonly IReadOnlyList<Vector2Int> Pos = new List<Vector2Int>();
		public static readonly IReadOnlyList<SpecialMove> SpecialMove = new List<SpecialMove>();
	}

}