﻿using System;
using System.Collections.Generic;
using imp.chess.game;
using UnityEngine;


namespace imp.chess {

	public static class ChessUtil {
		public static Vector2Int PosInvalid = new Vector2Int(-1, -1);
		
		/// Assuming this is a diagonal distance on the board, computes the distance
		public static int diagonalDist(this Vector2Int vec) => Math.Abs(vec.x) == Math.Abs(vec.y) ? Math.Abs(vec.x) : throw new Exception($"Non-diagonal vector: {vec}.");
		/// Calculates the manhattan distance, assuming this = something - somethingElse, where both somethings share a diagonal.
		public static int manhattan(this Vector2Int vec) => Math.Abs(vec.x) + Math.Abs(vec.y);

		/// The direction toward the enemy side of the board.
		public static Vector2Int Forward(bool white) => white ? Dirs.N : Dirs.S;
		

		//TODO use these everywhere then change the convention so X is col
		public static Vector2Int RowCol(int row, int col) => new Vector2Int(row, col);
		public static int Row(this Vector2Int pos) => pos.x;
		public static int Col(this Vector2Int pos) => pos.y;


		public static IReadOnlyList<Turn> CalculateTurnSequence(Faction white, Faction black) {
			var ret = new List<Turn>(4);
			ret.Add(game.Turn.White);
			if(white == Chess2.TwoKings)
				ret.Add(game.Turn.WhiteKing);
			ret.Add(game.Turn.Black);
			if(black == Chess2.TwoKings)
				ret.Add(game.Turn.BlackKing);
			return ret;
		}
	}
}