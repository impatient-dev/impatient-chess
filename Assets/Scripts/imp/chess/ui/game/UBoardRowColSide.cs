﻿using imp.chess.game;
using imp.util;
using TMPro;
using UnityEngine;

namespace imp.chess.ui.game {

	/// Puts letters/numbers next to each row and column of a board.
	public class UBoardRowColSide {
		/// Offset from the edge of the board.
		private static readonly float Off = 0.25f;
		
		private readonly GameObject Obj;
		private readonly AppRes R;

		public UBoardRowColSide(GameState state, GameObject parent, AppRes r) {
			Obj = parent.AddChild("Row/Col Markers");
			R = r;
			var board = state.Board.Value;
			for (var row = 0; row < board.Rows; row++) {
				AddLabel(row + .5f, - Off, ChessNotation.RowName(row), r);
				AddLabel(row + .5f, board.Cols + Off, ChessNotation.RowName(row), r);
			}
			for (var col = 0; col < board.Cols; col++) {
				AddLabel(- Off, col + .5f, ChessNotation.ColName(col), r);
				AddLabel(board.Rows + Off, col + .5f, ChessNotation.ColName(col), r);
			}
		}


		private GameObject AddLabel(float rowPos, float colPos, string text, AppRes r) {
			var ret = Obj.AddChild(text);
			ret.transform.position = new Vector3(colPos, rowPos, RD.SQUARE);
			ret.ARGText(r.Font.Main, r.Mat.TextWhite.color, .25f, text, style: FontStyles.Bold);
			return ret;
		}
	}

}