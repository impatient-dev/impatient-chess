﻿using UnityEngine;

namespace imp.chess.ui.game {

	/// Renders a chessboard, including pieces on it, the 2 sides, etc.
	public class UBoard {
		private readonly GameObject Obj;
		private readonly USides Sides;
		
		public UBoard(GameState state, AppRes r) {
			var board = state.Board.Value;
			Obj = new GameObject($"Board {board.Rows}x{board.Cols}");
			new UBoardRowColSide(state, Obj, r);
			new USquares(state, Obj, r);
			new UPieces(state, Obj, r);
			new USquareBorders(state, Obj, r);
			Sides = new USides(state, Obj, r);
			UMidline.CreateIfEnabled(board, Obj, r);
		}


		public void Destroy() {
			Object.Destroy(Obj);
			Sides.Destroy();
		}
	}

}