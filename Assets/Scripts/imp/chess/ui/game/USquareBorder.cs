﻿using imp.chess.game;
using JetBrains.Annotations;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.chess.ui.game {

	/// Draws borders around squares. The borders are slightly inside the squares, so adjacent squares can have different borders.
	public class USquareBorders {
		private readonly GameState State;
		private readonly GameObject Obj;
		private readonly AppRes R;
		private readonly USquareBorder[,] Children;

		public USquareBorders(GameState state, GameObject parent, AppRes r) {
			State = state;
			Obj = parent.AddChild("Borders");
			R = r;
			Children = new USquareBorder[state.Board.Value.Rows, state.Board.Value.Cols];
			state.Hover.PostChange(__ => Refresh());
			state.Selected.PostChangeAndImmediate(__ => Refresh());
		}


		private void Refresh() {
			foreach(var pos in State.Board.Value.Positions()) {
				ref USquareBorder child = ref Children[pos.Row(), pos.Col()];
				if (pos == State.Selected.Value)
					Show(ref child, pos, R.Mat.BorderSelected);
				else if (pos == State.Hover.Value)
					Show(ref child, pos, R.Mat.BorderHover);
				else if (State.SelectionPossibilities.Value.CanMoveTo(pos))
					Show(ref child, pos, MaterialForMoveTarget(State.Selected.Value, pos));
				else if(UiTargeting.SpecialTargeting(State.Selected.Value, pos, State.Board.Value, State.SelectionPossibilities.Value) != null)
					Show(ref child, pos, MaterialForMoveTarget(State.Selected.Value, pos));
				else if (child != null) {
					child.Visible = false;
					child.Name = "";
				}
			}
		}

		/// Creates it if it doesn't exist. Then sets the provided values and makes the child visible.
		private void Show([CanBeNull] ref USquareBorder child, Vector2Int pos, Material mat) {
			if (child == null)
				child = new USquareBorder(Obj);
			child.Pos = pos;
			child.Mat = mat;
			child.Visible = true;
			child.Name = pos.SquareName();
		}

		/// Determines the material the target of a move should have.
		private Material MaterialForMoveTarget(Vector2Int from, Vector2Int to) {
			var victim = State.Board.Value.At(to);
			if (!victim.Exists)
				return R.Mat.BorderMove;
			var attacker = State.Board.Value.At(from);
			return attacker.White == victim.White ? R.Mat.BorderFriendlyFire : R.Mat.BorderAttack;
		}
	}
	
	
	
	/// Initially invalid - you must assign a position and material immediately.
	public class USquareBorder {
		private static readonly float Width = .1f;
		private static float HalfW => Width / 2;

		private readonly GameObject _obj;
		private Vector2Int _pos;
		private readonly LineRenderer[] _renders;
		private Material _mat;

		public USquareBorder(GameObject parent) {
			_obj = parent.AddChild("LR");
			_renders = new[] { //top and bottom are full-width (X = 0 instead of HalfW, 1 instead of 1 - HalfW) so the corners look right
				CreateRenderer(0, HalfW, 1, HalfW), //bottom
				CreateRenderer(1 - HalfW, HalfW, 1 - HalfW, 1 - HalfW), //right
				CreateRenderer(1, 1 - HalfW, 0, 1 - HalfW), //top
				CreateRenderer(HalfW, 1 - HalfW, HalfW, HalfW), //left
			};
		}

		private LineRenderer CreateRenderer(float startX, float startY, float endX, float endY) {
			return _obj.AddChild("LR").ARLine(Width, _mat, V3(startX, startY, RD.MARKING), V3(endX, endY, RD.MARKING));
		}

		public Vector2Int Pos {
			get => _pos;
			set {
				if (value != _pos) {
					_obj.transform.localPosition = new Vector3(value.Col(), value.Row());
					_pos = value;
				}
			}
		}

		public Material Mat {
			get => _mat;
			set {
				if (value != _mat) {
					foreach (var r in _renders)
						r.material = value;
					_mat = value;
				}
			}
		}

		public string Name {
			set => _obj.name = value;
		}

		public bool Visible {
			set {
				_obj.SetActive(value);
			}
		}


		public void Destroy() => Object.Destroy(_obj);

	}

}