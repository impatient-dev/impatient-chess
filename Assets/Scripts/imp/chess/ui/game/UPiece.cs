﻿using System.Collections.Generic;
using imp.chess.game;
using imp.util;
using UnityEngine;
using static imp.chess.game.ClassicFaction;
using static imp.chess.game.Chess2;
using static imp.util.ImpatientUnityUtil;


namespace imp.chess.ui.game {

	public class UPieces {
		private readonly GameObject Obj;
		private readonly UPiece[,] Pieces;
		private readonly AppRes R;
		
		public UPieces(GameState state, GameObject parent, AppRes r) {
			Obj = parent.AddChild("Pieces");
			R = r;
			Pieces = new UPiece[state.Board.Value.Rows, state.Board.Value.Cols];
			state.Board.PostChangeAndImmediate(Refresh);
		}

		private void Refresh(Board board) {
			foreach(var pos in board.Positions()) {
				var actual = board.At(pos);
				ref UPiece piece = ref Pieces[pos.Row(), pos.Col()];
				if (actual.Exists) {
					if (piece == null || !piece.Piece.Equals(actual)) {
						piece?.Destroy();
						piece = new UPiece(board, pos, Obj, R);
					}
				} else {
					piece?.Destroy();
					piece = null;
				}
			};
		}
	}
	

	/// Represents a piece with text.
	public class UPiece {
		public readonly Piece Piece;
		private readonly GameObject Obj;

		public UPiece(Board board, Vector2Int pos, GameObject parent, AppRes r) {
			Obj = parent.AddChild(pos.SquareName()).LocalPos(V3(pos.Col() + .5f, pos.Row() + .5f, RD.PIECE));
			Piece = board.At(pos);
			Obj.ARGText(r.Font.Main, board.At(pos).White ? r.Mat.TextWhite.color : r.Mat.TextBlack.color, .5f, PieceName(Piece.Type));
		}

		public void Destroy() => Object.Destroy(Obj);

		private static readonly Dictionary<PieceType, string> TypeNames = new Dictionary<PieceType, string>().Also(names => {
			names[King] = names[UnmovedKing] = "K";
			names[Queen] = "Q";
			names[Rook] = names[UnmovedRook] = "R";
			names[Bishop] = "B";
			names[Knight] = "N";
			names[Pawn] = names[UnmovedPawn] = "P";

			names[NemesisQueen] = "nQ";
			names[NemesisPawn] = "nP";
			names[ElegantQueen] = "eQ";
			names[EmpoweredBishop] = "eB";
			names[EmpoweredKnight] = "eN";
			names[EmpoweredRook] = "eR";
			names[WarriorKing] = "wK";
			names[ReaperQueen] = "rQ";
			names[ReaperGhost] = "rG";
			names[JungleQueen] = "aQ";
			names[Tiger] = "aB";
			names[WildHorse] = "aN";
			names[Elephant] = "aR";
		});

		private static string PieceName(PieceType type) => TypeNames[type] ?? "?";
	}

}