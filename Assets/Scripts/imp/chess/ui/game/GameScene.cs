﻿using imp.chess.game;
using imp.chess.ui.game.overlay;
using UnityEngine;
using static imp.chess.ChessUtil;
using static imp.util.ImpatientUnityUtil;


namespace imp.chess.ui.game {

	/// Manages the UI for running a game of chess, updating the state (and UI) as the user takes actions.
	public class GameScene : AppScene {
		private readonly GameState State;
		private readonly UBoard Board;
		private readonly UBoardOverlay Overlay;
		public AppMainScript Script { get; set; }

		public GameScene(GameState state, AppRes r) {
			State = state;
			Board = new UBoard(state, r);
			Overlay = new UBoardOverlay(state, r);
		}


		public void Update() {
			if (PointingAtUi())
				State.Hover.Value = PosInvalid;
			else
				State.Hover.Value = GetMouseSquare();
		}

		public void OnClick(int button) {
			if (PointingAtUi())
				return;
			if (button == 0) {
				if (State.Winner.Value.HasValue)
					return;
				if (State.Board.Value.Contains(State.Selected.Value)) {
					TryMakeMove(State.Selected.Value, GetMouseSquare());
				} else {
					State.Selected.Value = GetMouseSquare();
					Debug.Log($"Selected {State.Selected.Value.SquareName()}.");
				}
			} else if (button == 1) {
				State.Selected.Value = PosInvalid;
			}
		}


		public void OnKeyDown(KeyCode code) {
			if(code == KeyCode.Space && State.Turn.Value is Turn.KingTurn)
				State.MakeMove(null);
		}

		private Vector2Int GetMouseSquare() {
			var worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2Int ret = USquare.SquareContaining(worldPos);
			return State.Board.Value.Contains(ret) ? ret : PosInvalid;
		}

		private void TryMakeMove(Vector2Int from, Vector2Int to) {
			if (State.SelectionPossibilities.Value.CanMoveTo(to)) {
				Debug.Log($"Moving {from.SquareName()}-{to.SquareName()}.");
				State.MakeMove(new NormalMove(from, to));
				return;
			}

			var special = UiTargeting.SpecialTargeting(from, to, State.Board.Value, State.SelectionPossibilities.Value);
			if (special != null) {
				Debug.Log($"Making special move '{special.Name}' for {from.SquareName()}-{to.SquareName()}.");
				State.MakeMove(special);
				return;
			}

			State.Selected.Value = PosInvalid;
		}

		public void Destroy() {
			Board.Destroy();
			Overlay.Destroy();
		}
	}

}