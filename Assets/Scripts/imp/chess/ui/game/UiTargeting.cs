using imp.chess.game;
using JetBrains.Annotations;
using UnityEngine;

namespace imp.chess.ui.game {
	public class UiTargeting {
		/// Given the user is trying to move the piece at "from", this function returns which special move would be triggered by clicking on "to", if any.
		[CanBeNull] public static SpecialMove SpecialTargeting(Vector2Int from, Vector2Int to, Board board, PiecePossibilities possibilities) {
			if (!board.Atx(from).Exists || !board.Contains(to))
				return null;
			
			
			if (board.At(from).Type == Chess2.WarriorKing) {
				if (!from.Equals(to))
					return null;
				var sFrom = possibilities.SpecialsFrom(from);
				return sFrom.Count == 1 ? sFrom[0] : null;
			}

			var specials = possibilities.SpecialsTargeting(to);
			return specials.Count == 1 ? specials[0] : null;
		}
	}
}