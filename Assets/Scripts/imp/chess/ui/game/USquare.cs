﻿using imp.chess.game;
using imp.util;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;


namespace imp.chess.ui.game {

	/// Displays the squares of a game board, but not pieces or highlight.
	public class USquares {
		private readonly GameObject Obj;
		
		public USquares(GameState state, GameObject parent, AppRes r) {
			Obj = parent.AddChild("Squares");
			foreach (var pos in state.Board.Value.Positions())
				new USquare(pos, Obj, r);
		}
	}
	

	/// Represents a square of the game board. The lower left of the square is at (col,row).
	public class USquare {
		public readonly int Row, Col;
		private readonly GameObject Obj;
		private Piece _piece = Piece.NONE;
		
		private static readonly Mesh SharedMesh = new Mesh().Also(mesh => {
			mesh.vertices = new[] { new Vector3(0, 0), new Vector3(0, 1), new Vector3(1, 1), new Vector3(1, 0) };
			mesh.triangles = new[] { 0,1,2, 2,3,0 };
			// mesh.uv = new Vector2[4];
		});

		public USquare(Vector2Int pos, GameObject parent, AppRes r) {
			Row = pos.Row();
			Col = pos.Col();
			Obj = parent.AddChild(V2I(Row, Col).SquareName());
			Obj.transform.position = new Vector3(Col, Row, RD.SQUARE);

			Obj.AR<MeshFilter>().mesh = SharedMesh;
			Obj.AR<MeshRenderer>().material = (Row + Col) % 2 == 0 ? r.Mat.SquareDark : r.Mat.SquareLight;
		}
		
		
		public static Vector2Int SquareContaining(Vector2 pos) => new Vector2Int(Mathf.FloorToInt(pos.y), Mathf.FloorToInt(pos.x));
	}

}