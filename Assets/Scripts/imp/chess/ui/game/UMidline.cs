using imp.chess.game;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.chess.ui.game {
	public class UMidline {
		/// Draws a line on the middle of the board, but only if midline invasion is enabled.
		public static void CreateIfEnabled(Board board, GameObject parent, AppRes r) {
			if (!board.Rules.MidlineInvasion)
				return;
			var row = board.Rows / 2;
			parent.AddChild("Midine").ARLine(.1f, r.Mat.Midline).Positions(V3(0, row, RD.MARKING), V3(board.Cols, row, RD.MARKING));
		}
	}
}