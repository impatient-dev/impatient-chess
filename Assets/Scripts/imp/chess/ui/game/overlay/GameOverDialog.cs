using imp.chess.game;
using imp.util;
using UnityEngine;
using UnityEngine.UI;
using static imp.util.ImpatientUtil;

namespace imp.chess.ui.game.overlay {
	/// A simple dialog to show when the game ends due to someone winning. 
	public class GameOverDialog : AppDialog {
		private readonly GameObject Root;

		public GameOverDialog(GameWinner winner, GameObject canvas, AppRes r) {
			Root = canvas.AddChild("GameOverDialog").AppPanelV();
			var uTitle = Root.AddChild("Title").WHorizontal().ARCText(r.Mat.Text).AppH2(r);
			var uDescription = Root.AddChild("Description").WHorizontal().ARCText(r.Mat.Text).AppBody(r);

			string winColor = winner.White.WhiteStr(), loseColor = (!winner.White).WhiteStr();
			string title = "", description = "";
			switch (winner.Reason) {
				case WinReason.Capture:
					title = $"{loseColor}'s King Captured";
					description = $"{loseColor} lost because their king was captured.";
					break;
				case WinReason.Checkmate:
					title = $"{loseColor} Checkmated";
					description = $"{loseColor} is in checkmate and has lost the game.";
					break;
				case WinReason.Stalemate:
					title = $"{loseColor} Stalemated";
					description = $"{loseColor} has lost because they have no valid moves (excluding moves that would leave them in check).";
					break;
				case WinReason.MidlineInvasion:
					title = $"{winColor} Midline Invasion";
					description = $"{winColor} has won the game because their king(s) advanced past the middle of the board.";
					break;
				default:
					UnexpectedType(winner.Reason);
					break;
			}

			uTitle.text = title;
			uDescription.text = description;

			Root.AddChild("Close").WHorizontal().AppBtn(r, "Close", Destroy);
		}


		public void Destroy() {
			Root.Destroy();
		}
	}
}