using imp.chess.game;
using imp.util;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace imp.chess.ui.game.overlay {
	/// Overlays a canvas over the whole screen.
	/// Normally the canvas shows nothing, but it can show popup windows when necessary.
	public class UBoardOverlay {
		private readonly GameObject Root = new GameObject("BoardOverlay").WCanvas().WHorizontal();
		private readonly AppRes R;
		[CanBeNull] private AppDialog Dialog;


		public UBoardOverlay(GameState state, AppRes r) {
			Root.AR<EventSystem>();
			Root.AR<StandaloneInputModule>();
			R = r;
			state.Winner.PostChange(OnWinner);
		}

		private void OnWinner(GameWinner? winner) {
			if (winner == null)
				return;
			new GameOverDialog(winner.Value, Root, R);
		}

		public void Destroy() => Root.Destroy();
	}



	/// TODO
	public interface AppDialog {
		void Destroy();
	}
}