﻿using TMPro;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;
using static imp.util.ImpatientUtil;


namespace imp.chess.ui.game {

	/// Displays the 2 factions that are fighting, and whose turn it is.
	public class USides {
		/// Offset from the board edge.
		private static readonly float Off = 1f;
		private static readonly float TextSize = 0.5f;
		
		private readonly GameState State;
		private readonly AppRes R;
		private readonly TextMeshPro White, Black;
		
		public USides(GameState state, GameObject parent, AppRes r) {
			State = state;
			R = r;
			var board = state.Board.Value;
			White = parent.AddChild("White").LocalPos(V3(board.Cols / 2f, -Off)).ARGText(r.Font.Main, r.Mat.TextWhite.color, TextSize, state.White.Name);
			Black = parent.AddChild("Black").LocalPos(V3(board.Rows / 2f, board.Rows + Off)).ARGText(r.Font.Main, r.Mat.TextBlack.color, TextSize, state.Black.Name);
			state.Turn.PostChangeAndImmediate(__ => Refresh());
			state.Winner.PostChange(__ => Refresh());
		}

		private void Refresh() {
			if (State.Winner.Value.HasValue) {
				TextMeshPro winner = White, loser = Black;
				if (!State.Winner.Value.Value.White)
					Swap(ref winner, ref loser);
				winner.fontStyle = FontStyles.Bold;
				loser.fontStyle = FontStyles.Bold | FontStyles.Strikethrough | FontStyles.Italic;
			} else {
				TextMeshPro active = White, passive = Black;
				if (!State.Turn.Value.IsWhite)
					Swap(ref active, ref passive);
				active.fontStyle = FontStyles.Bold | FontStyles.Underline;
				passive.fontStyle = FontStyles.Bold;
			}
		}

		public void Destroy() {
			White.gameObject.Destroy();
			Black.gameObject.Destroy();
		}
	}
	
}