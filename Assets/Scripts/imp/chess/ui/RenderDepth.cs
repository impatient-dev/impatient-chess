﻿namespace imp.chess.ui {

	/// Render Depths: specifies the Z coordinate different things should be rendered at. Lower Z coordinates appear closer to the camera.
	public class RD {
		public static readonly float SQUARE = 0F;
		/// Lines, annotations, etc.
		public static readonly float MARKING = -1F;
		public static readonly float PIECE = -2F;
	}

}