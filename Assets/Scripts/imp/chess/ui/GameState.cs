﻿using System.Collections.Generic;
using imp.chess.game;
using JetBrains.Annotations;
using UnityEngine;
using static imp.chess.ChessUtil;
using static imp.chess.game.CheckUtil;
using static imp.util.ImpatientUtil;


namespace imp.chess.ui {

	/// What the UI needs to know about the current game in progress.
	/// Vectors are POS_INVALID instead of null; check for this by checking whether the board contains the position.
	public class GameState {
		public readonly Faction White, Black;
		public readonly IReadOnlyList<Turn> TurnSequence;
		
		/// Whose turn it currently is, from the turn sequence. Ignore if the game has a winner.
		public readonly ImpProp<Turn> Turn;
		/// Current position in the turn sequence.
		private int TurnIdx = 0;
		public readonly ImpProp<Board> Board;
		/// Whether the player whose turn it is is in check.
		public readonly ImpProp<bool> Check = new ImpProp<bool>(false);
		public readonly ImpProp<GameWinner?> Winner = new ImpProp<GameWinner?>(null);

		/// What square the user is hovering over.
		public readonly ImpProp<Vector2Int> Hover = new ImpProp<Vector2Int>(PosInvalid);
		/// What piece the user has selected to move.
		public readonly ImpProp<Vector2Int> Selected = new ImpProp<Vector2Int>(PosInvalid);
		/// What the selected piece can do - recalculated every time the selection changes.
		public readonly ImpProp<PiecePossibilities> SelectionPossibilities = new ImpProp<PiecePossibilities>(PiecePossibilities.Empty);

		public GameState(BoardSetup setup, Turn turn, Board board) {
			White = setup.White;
			Black = setup.Black;
			TurnSequence = CalculateTurnSequence(White, Black);
			Turn = new ImpProp<Turn>(turn);
			Board = new ImpProp<Board>(board);
			Selected.PostChange(pos => SelectionPossibilities.Value = PiecePossibilities.Calculate(Board.Value, Turn.Value, pos));
		}


		/// Applies the move to the board, then advances to the next turn (possibly ending the game if this is a winning move).
		/// To skip a turn, call this function with a null argument.
		public void MakeMove([CanBeNull] Move move) {
			PostMove postMove = null;

			if (move != null) {
				var board = Board.Value.Clone();
				move.Apply(board, out postMove);
				if (Turn.Value.IsNormal)
					board.EnPassantCol = move.EnPassantCol;
				Board.Value = board;
			}

			TurnIdx = (TurnIdx + 1) % TurnSequence.Count;
			Turn.Value = TurnSequence[TurnIdx];
			Selected.Value = PosInvalid;
				
			if (postMove != null) {
				if (postMove is PostMoveLoss loss)
					Winner.Value = new GameWinner(!loss.WhiteVictim, WinReason.Capture);
				else if (postMove is PostMoveMidlineInvasion inv)
					Winner.Value = new GameWinner(inv.WhiteVictor, WinReason.MidlineInvasion);
				else if (postMove is PostMovePromotion promote)
					TODO();
				else
					UnexpectedType(postMove);
			}
			
			if(Winner.Value == null) {
				Check.Value = IsInCheck(Board.Value, Turn.Value.IsWhite);
				if (!HasAnyValidMove(Board.Value, Turn.Value.IsWhite)) {
					var reason = Check.Value ? WinReason.Checkmate : WinReason.Stalemate;
					Winner.Value = new GameWinner(!Turn.Value.IsWhite, reason);
				}
			}
		}
	}
}