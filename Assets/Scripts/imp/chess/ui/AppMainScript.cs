using imp.util;
using UnityEngine;

namespace imp.chess.ui {
	/// A set of things that can be viewed and interacted with, such as a chess game or the main menu.
	/// A scene is created in its constructor, and can be destroyed when it's time to show a new scene.
	public interface AppScene {
		/// This will be set before other scene methods are called.
		AppMainScript Script { set; }
		/// Called once each frame.
		void Update();
		/// Called when the user presses a mouse button. The first button is 0.
		/// Note: this will be called for any click, regardless of location. To check if the user clicked on the UI vs the world, call EventSystem.current.IsPointerOverGameObject()
		void OnClick(int button);
		void OnKeyDown(KeyCode key);
		void Destroy();
	}



	/// Shows 1 scene at a time, forwarding mouse & keyboard events to the active scene.
	/// This exists so we can switch between a game and the main menu.
	public class AppMainScript : MonoBehaviour {
		private AppScene Scene;

		public static void Create(AppScene initialScene) {
			GameObject obj = new GameObject("Script");
			var script = obj.AR<AppMainScript>();
			script.useGUILayout = false;
			script.SwitchTo(initialScene);
		}

		public void SwitchTo(AppScene newScene) {
			Scene?.Destroy();
			Scene = newScene;
			Scene.Script = this;
		}
		

		void Update() {
			for (int button = 0; button <= 1; button++)
				if (Input.GetMouseButtonDown(button))
						Scene.OnClick(button);
			Scene.Update();
		}

		void OnGUI() {
			var ev = Event.current;
			switch (ev.type) {
				case EventType.KeyDown:
					Scene.OnKeyDown(ev.keyCode);
					break;
			}
		}
	}
}