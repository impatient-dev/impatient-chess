﻿using TMPro;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.chess.ui {

	/// Loads all Unity resources (fonts, materials, etc.) used by this game, and maintains references to them.
	/// Construction will fail if any resources are missing.
	public sealed class AppRes {
		public readonly AppFonts Font = new AppFonts();
		public readonly AppMaterials Mat = new AppMaterials();
	}

	public sealed class AppFonts {
		//TODO choose either Pro or regular, then delete the other one
		public readonly Font MainLegacy = Resources.GetBuiltinResource<Font>("Arial.ttf");
//		public readonly Font MainLegacy = LoadResource<Font>("Fonts/GNU_FreeFont/FreeSans");
		public readonly TMP_FontAsset Main = Resources.GetBuiltinResource<Font>("Arial.ttf").Pro();
//		public readonly TMP_FontAsset Main = LoadResource<Font>("Fonts/GNU_FreeFont/FreeSans").Pro();
	}

	public sealed class AppMaterials {
		/// Must be used for all UI text - otherwise, TextMeshPro can render text behind world objects (even in an overlay canvas - WTF).
		public readonly Material Text = LoadResource<Material>("Materials/Text");
		
		public readonly Material BorderAttack = LoadResource<Material>("Materials/BorderAttack");
		public readonly Material BorderFriendlyFire = LoadResource<Material>("Materials/BorderFriendlyFire");
		public readonly Material BorderHover = LoadResource<Material>("Materials/BorderHover");
		public readonly Material BorderMove = LoadResource<Material>("Materials/BorderMove");
		public readonly Material BorderSelected = LoadResource<Material>("Materials/BorderSelected");
		
		public readonly Material Midline = LoadResource<Material>("Materials/Midline");
		
		public readonly Material SquareDark = LoadResource<Material>("Materials/SquareDark");
		public readonly Material SquareLight = LoadResource<Material>("Materials/SquareLight");
		
		public readonly Material TextBlack = LoadResource<Material>("Materials/TextBlack");
		public readonly Material TextWhite = LoadResource<Material>("Materials/TextWhite");
	}
}