using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using static imp.util.ImpatientUnityUtil;
using TextPro = TMPro.TextMeshProUGUI;

namespace imp.chess.ui {
	/// Contains hardcoded style information, such as text colors and sizes, for use by the UI.
	/// In-world text is not covered by this class - only menus, dialogs, etc. are covered.
	public static class AppStyle {
		public static readonly Color UiBack = UColor(.3f);
		public static readonly Color UiFront = UColor(.8f);
		
		private static readonly ColorBlock BtnColors = new ColorBlock() {
			normalColor = UColor(.2f),
			highlightedColor = UColor(.1f),
			selectedColor = UColor(.15f),
			pressedColor = new Color(0f, 0f, .4f),
			colorMultiplier = 1,
			fadeDuration = 0,
		};
		
		public static readonly RectOffset PanelPadding = new RectOffset(5, 5, 5, 5);


		public static GameObject AppPanelV(this GameObject obj) {
			var layout = obj.WBackground(UiBack).AR<VerticalLayoutGroup>().Set();
			layout.padding = PanelPadding;
			return obj;
		}


		//TODO delete
		public static Text Align(this Text text, TextAnchor alignment = TextAnchor.MiddleCenter) {
			text.alignment = alignment;
			return text;
		}

		public static TextMeshProUGUI Align(this TextMeshProUGUI text, TextAlignmentOptions alignment = TextAlignmentOptions.Center) {
			text.alignment = alignment;
			return text;
		}

		//TODO delete
		public static Text Font(this Text text, Font font, int fontSize, Color color, FontStyle style = FontStyle.Normal) {
			text.font = font;
			text.fontSize = fontSize;
			text.color = color;
			text.fontStyle = style;
			return text;
		}

		public static TextMeshProUGUI Font(this TextMeshProUGUI text, TMP_FontAsset font, float size, Color color, FontStyles style = FontStyles.Normal) {
			text.fontSize = size;
			text.font = font;
			text.color = color;
			text.fontStyle = style;
			return text;
		}


		public static TextPro AppH1(this TextPro text, AppRes r) => text.Align(TextAlignmentOptions.Center).Font(r.Font.Main, 24, UiFront, FontStyles.Bold);
		public static TextPro AppH2(this TextPro text, AppRes r) => text.Align(TextAlignmentOptions.Center).Font(r.Font.Main, 16, UiFront, FontStyles.Bold);
		public static TextPro AppH3(this TextPro text, AppRes r) => text.Align(TextAlignmentOptions.Center).Font(r.Font.Main, 12, UiFront, FontStyles.Bold);
		
		public static TextPro AppBody(this TextPro text, AppRes r) => text.Align(TextAlignmentOptions.Left).Font(r.Font.Main, 12, UiFront);



		/// Adds a variety of components, and a child. You should not mess with this object further, including adding a layout (we handle that).
		public static Button AppBtn(this GameObject obj, AppRes r, string text, UnityAction onClick) {
			var btn = obj.AR<Button>();
			btn.transition = Selectable.Transition.ColorTint;
			btn.colors = BtnColors;
			btn.targetGraphic = obj.AR<Image>();
			btn.gameObject.AddChild("Text").ARCText(r.Mat.Text).text = text;
			btn.onClick.AddListener(onClick);
			return btn;
		}
	}
}