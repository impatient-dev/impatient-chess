using System.Collections.Generic;
using imp.util;
using static imp.chess.game.ClassicFaction;
using static imp.chess.game.Chess2;

namespace imp.chess.game {
	public class PieceTypeInfo {
		private static readonly Dictionary<PieceType, PieceTypeInfo> Map = new Dictionary<PieceType, PieceTypeInfo>();
		public static readonly PieceTypeInfo Default = new PieceTypeInfo(null, "???", "The description for this piece is missing.");

		public readonly PieceType Type;
		public readonly string Name;
		public readonly string Description;

		private PieceTypeInfo(PieceType type, string name, string description = "") {
			Type = type;
			Name = name;
			Description = description;
		}


		/// Returns a default 'unknown-piece' info if we don't know about this piece.
		public static PieceTypeInfo Get(PieceType type) => Map.Opt(type) ?? Default;
		
		private static void Add(PieceTypeInfo info) => Map.PutNew(info.Type, info);
		
		static PieceTypeInfo() {
			Add(new PieceTypeInfo(King, "King"));
			Add(new PieceTypeInfo(Queen, "Queen"));
			Add(new PieceTypeInfo(Bishop, "Bishop"));
			Add(new PieceTypeInfo(Knight, "Knight"));
			Add(new PieceTypeInfo(Rook, "Rook"));
			Add(new PieceTypeInfo(Pawn, "Pawn"));
			Add(new PieceTypeInfo(UnmovedKing, "King", "This piece has not moved yet, and is still able to castle."));
			Add(new PieceTypeInfo(UnmovedRook, "Rook", "This piece has not moved yet, and is still able to castle."));
			Add(new PieceTypeInfo(UnmovedPawn, "Pawn"));
			
			//TODO Chess2 pieces
		}
	}
}