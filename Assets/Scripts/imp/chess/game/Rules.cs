namespace imp.chess.game {
	/// Defines any special rules that are in effect for a game.
	/// This object is mutable, but once a game starts and this is associated with a Board, you should not change the rules.
	public class Rules {
		public bool MidlineInvasion = true;
	}
}