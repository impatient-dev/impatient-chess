﻿using System;
using System.Collections.Generic;
using UnityEngine;
using static imp.chess.ChessUtil;
using static imp.util.ImpatientUtil;
using static imp.util.ImpatientUnityUtil;


namespace imp.chess.game {

	public static class ClassicFaction {
		public static readonly Movement KingMovement = new ComboMovement(OrthoMovement.ONE, DiagMovement.ONE);
		
		public static readonly PieceType King = new PieceType(KingMovement, NoSpecialMoves.INSTANCE, PTTag.Essential);
		public static readonly PieceType Queen = new PieceType(new ComboMovement(OrthoMovement.UNLIMITED, DiagMovement.UNLIMITED), NoSpecialMoves.INSTANCE);
		public static readonly PieceType Bishop = new PieceType(DiagMovement.UNLIMITED, NoSpecialMoves.INSTANCE);
		public static readonly PieceType Knight = new PieceType(new LMovement(2, 1), NoSpecialMoves.INSTANCE);
		public static readonly PieceType Rook = new PieceType(OrthoMovement.UNLIMITED, NoSpecialMoves.INSTANCE);
		public static readonly PieceType Pawn = new PieceType(PawnMovement.INSTANCE, MovedPawnSpecial.INSTANCE, PTTag.Promotes);
		
		public static readonly PieceType UnmovedKing = new PieceType(
			KingMovement, NormalDefense.INSTANCE, CastlingSpecial.INSTANCE, King, PTTag.Essential);
		public static readonly PieceType UnmovedRook = new PieceType(
			OrthoMovement.UNLIMITED, NoSpecialMoves.INSTANCE);
		public static readonly PieceType UnmovedPawn = new PieceType(
			PawnMovement.INSTANCE, NormalDefense.INSTANCE,UnmovedPawnSpecial.INSTANCE, Pawn, PTTag.Promotes);
		
		public static readonly Faction INSTANCE = new Faction("Classic", UnmovedKing, Queen, Bishop, Knight, UnmovedRook, UnmovedPawn);
	}
	
	

	/// Moves the king 2 spaces toward a rook, and places the rook on the skipped space.
	/// TODO forbid castling the king out of, through, or into check.
	public class CastlingSpecial : SpecialMoveFinder {
		public static readonly CastlingSpecial INSTANCE = new CastlingSpecial();
		private static readonly int MIN_DISTANCE = 2;

		public CastlingSpecial() : base(2) {}

		public override IEnumerable<SpecialMove> Find(Board board, Vector2Int pos) {
			for(var tgt = pos + V2I(0, 1); board.Contains(tgt); tgt += V2I(0, 1)) {
				var piece = board.At(tgt);
				if (piece.Exists) {
					if (piece.Type == ClassicFaction.UnmovedRook && (tgt - pos).y >= MIN_DISTANCE) {
						var kingTo = pos + V2I(0, 2);
						var rookTo = pos + V2I(0, 1);
						yield return new SpecialMove(pos, "Castle (kingside)", Empty.Pos, 
							ListOf(new NormalMove(pos, kingTo), new NormalMove(tgt, rookTo)));
					}
					break;
				}
			}

			for(var tgt = pos - V2I(0, 1); board.Contains(tgt); tgt -= V2I(0, 1)) {
				var piece = board.At(tgt);
				if (piece.Exists) {
					if (piece.Type == ClassicFaction.UnmovedRook && (pos - tgt).y >= MIN_DISTANCE) {
						var kingTo = pos - V2I(0, 2);
						var rookTo = pos - V2I(0, 1);
						yield return new SpecialMove(pos, "Castle (queenside)", Empty.Pos, 
							ListOf(new NormalMove(pos, kingTo), new NormalMove(tgt, rookTo)));
					}
					break;
				}
			}
		}
	}



	public class PawnMovement : YieldMovement {
		public static readonly PawnMovement INSTANCE = new PawnMovement();
		public PawnMovement() : base(3) {}
		
		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			var white = board.At(from).White;
			var fwd = Forward(white);
			
			if(!board.At(from + fwd).Exists)
				yield return from + fwd;
			foreach (var side in Dirs.HORIZONTAL) {
				var tgt = from + fwd + side;
				if(board.Contains(tgt) && board.At(tgt).Exists && board.At(tgt).White != white && board.At(tgt).Type.Defense.IsVulnerable(board, from, tgt))
					yield return tgt;
			}
		}
	}



	/// Handles en passant capture.
	public class MovedPawnSpecial : SpecialMoveFinder {
		public static readonly MovedPawnSpecial INSTANCE = new MovedPawnSpecial();
		public MovedPawnSpecial() : base(1) {}

		public override IEnumerable<SpecialMove> Find(Board board, Vector2Int pos) {
			if (board.EnPassantCol == -1 || Math.Abs(board.EnPassantCol - pos.y) != 1 || pos.x != (board.At(pos).White ? board.RowMax - 3 : 3))
				yield break;
			var fwd = Forward(board.At(pos).White);
			yield return new SpecialMove(pos, "en passant", 
				ListOf(V2I(pos.x, board.EnPassantCol)), 
				ListOf(new NormalMove(pos, V2I(pos.x + fwd.x, board.EnPassantCol)))
			);
		}
	}


	/// Handles the pawn initial double-move. Does not handle en passant capture, because a pawn has to move to be able to capture en passant.
	public class UnmovedPawnSpecial : SpecialMoveFinder {
		public static readonly UnmovedPawnSpecial INSTANCE = new UnmovedPawnSpecial();
		public UnmovedPawnSpecial() : base(1) {}

		public override IEnumerable<SpecialMove> Find(Board board, Vector2Int pos) {
			var fwd = Forward(board.At(pos).White);
			if (!board.At(pos + fwd).Exists && !board.At(pos + fwd + fwd).Exists)
				yield return new SpecialMove(pos, "pawn double move", Empty.Pos, ListOf(new NormalMove(pos, pos + fwd + fwd)), pos.Col());
		}
	}
}