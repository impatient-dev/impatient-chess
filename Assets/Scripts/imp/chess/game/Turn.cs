﻿namespace imp.chess.game {

	/// Represents whose turn it is, telling you what pieces can move.
	public abstract class Turn {
		public readonly bool IsWhite;
		/// False for the "king turn" used by Two Kings.
		public readonly bool IsNormal;
		
		public abstract bool CanMove(in Piece piece);

		protected Turn(bool isWhite, bool isNormal) {
			IsWhite = isWhite;
			IsNormal = isNormal;
		}

		public static readonly Turn White = new NormalTurn(true);
		public static readonly Turn Black = new NormalTurn(false);
		public static readonly Turn WhiteKing = new KingTurn(true);
		public static readonly Turn BlackKing = new KingTurn(false);

		public class NormalTurn : Turn {
			public NormalTurn(bool isWhite) : base(isWhite, true) { }
			public override bool CanMove(in Piece piece) => IsWhite == piece.White;
		}

		public class KingTurn : Turn {
			public KingTurn(bool isWhite) : base(isWhite, false) { }
			public override bool CanMove(in Piece piece) => piece.Type.Essential && IsWhite == piece.White;
		}
	}
}