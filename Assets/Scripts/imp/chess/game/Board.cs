﻿using System;
using System.Collections.Generic;
using imp.util;
using JetBrains.Annotations;
using UnityEngine;


namespace imp.chess.game {

	/// Mutable. Vector positions are (row, col), and range from (0,0) to (Rows - 1, Cols - 1).
	public class Board {
		public readonly int Rows;
		public readonly int Cols;
		public int RowMax => Rows - 1;
		public int ColMax => Cols - 1;

		public readonly Rules Rules;
		/// The column where an en passant capture can happen, or -1 if no pawn moved 2 squares in the previous turn.
		public int EnPassantCol;
		
		private readonly Piece[,] Pieces;

		/// Returns false for positions off the board.
		public bool Contains(Vector2Int pos) => pos.Row() >= 0 && pos.Row() <= RowMax && pos.Col() >= 0 && pos.Col() <= ColMax;

		/// Returns a reference to the piece at a board position, possible NONE. The position must exist on the board.
		public ref Piece At(Vector2Int pos) {
			if(!Contains(pos))
				throw new Exception($"({pos.x},{pos.y}) invalid on a {Rows}x{Cols} board.");
			return ref Pieces[pos.x, pos.y];
		}

		/// Returns NONE of there nothing at the position, or if the position is off the board. "x" = "expanded".
		public Piece Atx(Vector2Int pos) {
			if(!Contains(pos))
				return Piece.NONE;
			return Pieces[pos.x, pos.y];
		}

		public IEnumerable<Vector2Int> Positions() {
			for(var row = 0; row < Rows; row++)
				for (var col = 0; col < Cols; col++)
					yield return new Vector2Int(row, col);
					
		}

		private Board(int rows, int cols, Piece[,] pieces, Rules rules, int enPassantCol) {
			Rows = rows;
			Cols = cols;
			Pieces = pieces;
			Rules = rules;
			EnPassantCol = enPassantCol;
		}

		public static Board Empty(Rules rules, int rows = 8, int cols = 8) {
			var arr = new Piece[rows, cols];
			for(var r = 0; r < rows; r++)
				for(var c = 0; c < cols; c++)
					arr[r, c] = Piece.NONE;
			return new Board(rows, cols, arr, rules, -1);
		}

		public Board Clone() {
			var array = (Piece[,]) Pieces.Clone();
			return new Board(Rows, Cols, array, Rules, EnPassantCol);
		}

		/// Clones the board, then applies this move.
		/// WARNING: this method will discard any PostMove information; this is acceptable for figuring out check, but generally not for other purposes.
		public Board With(Move move) => Clone().Also(copy => move.Apply(copy, out var postMove));

		/// Moves a piece to a new position, replacing any existing piece.
		public void Move(Vector2Int from, Vector2Int to) {
			var src = At(from);
			At(to) = new Piece(src.Type.TypePostMove, src.White);
			At(from) = Piece.NONE;
		}
	}


	public readonly struct Piece {
		public static readonly Piece NONE = new Piece(null, false);
		
		/// If this is null, all other properties should be ignored.
		[CanBeNull] public readonly PieceType Type;
		public readonly bool White;
		public bool Exists => Type != null;

		public Piece(PieceType type, bool white) {
			Type = type;
			White = white;
		}
	}
}