﻿using static imp.util.ImpatientUnityUtil;


namespace imp.chess.game {

	/// Instructions for creating a game/board.
	public class BoardSetup {
		public Faction White = ClassicFaction.INSTANCE;
		public Faction Black = ClassicFaction.INSTANCE;
		public readonly Rules Rules = new Rules();
	}

	public static class Setup {
		public static Board Create(this BoardSetup setup) {
			var ret = Board.Empty(setup.Rules);

			var r = 0;
			var c = 0;
			ret.At(V2I(r, c++)) = new Piece(setup.White.Rook, true);
			ret.At(V2I(r, c++)) = new Piece(setup.White.Knight, true);
			ret.At(V2I(r, c++)) = new Piece(setup.White.Bishop, true);
			ret.At(V2I(r, c++)) = new Piece(setup.White.Queen, true);
			ret.At(V2I(r, c++)) = new Piece(setup.White.King, true);
			ret.At(V2I(r, c++)) = new Piece(setup.White.Bishop, true);
			ret.At(V2I(r, c++)) = new Piece(setup.White.Knight, true);
			ret.At(V2I(r, c++)) = new Piece(setup.White.Rook, true);

			r = ret.RowMax;
			c = 0;
			ret.At(V2I(r, c++)) = new Piece(setup.Black.Rook, false);
			ret.At(V2I(r, c++)) = new Piece(setup.Black.Knight, false);
			ret.At(V2I(r, c++)) = new Piece(setup.Black.Bishop, false);
			ret.At(V2I(r, c++)) = new Piece(setup.Black.Queen, false);
			ret.At(V2I(r, c++)) = new Piece(setup.Black.King, false);
			ret.At(V2I(r, c++)) = new Piece(setup.Black.Bishop, false);
			ret.At(V2I(r, c++)) = new Piece(setup.Black.Knight, false);
			ret.At(V2I(r, c++)) = new Piece(setup.Black.Rook, false);

			for (var col = 0; col <= ret.ColMax; col++) {
				ret.At(V2I(1, col)) = new Piece(setup.White.Pawn, true);
				ret.At(V2I(ret.RowMax - 1, col)) = new Piece(setup.White.Pawn, false);
			}

			return ret;
		}
	}

}