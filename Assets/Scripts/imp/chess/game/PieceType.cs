﻿using System.Linq;
using JetBrains.Annotations;


namespace imp.chess.game {

	/// There should only be 1 instance of each type.
	public class PieceType {
		/// For kings - you lose if this piece is captured.
		public readonly Movement Move;
		public readonly Defense Defense;
		public readonly SpecialMoveFinder Special;
		
		/// What type this piece becomes after it moves.
		/// This is usually the same as the original type, except for pieces that lose abilities after their first move.
		public readonly PieceType TypePostMove;

		public readonly bool Essential;
		public readonly bool Promotes;
		public readonly bool Ranged;

		/// For the type-post-move, null means to use the existing type (which is what most piece types want).
		public PieceType(Movement move, Defense defense, SpecialMoveFinder special, [CanBeNull] PieceType typePostMove, params PTTag[] tags) {
			Move = move;
			Defense = defense;
			Special = special;
			TypePostMove = typePostMove ?? this;
			
			Essential = tags.Contains(PTTag.Essential);
			Promotes = tags.Contains(PTTag.Promotes);
			Ranged = tags.Contains(PTTag.Ranged);
		}
		
		public PieceType(Movement move, SpecialMoveFinder special, params PTTag[] tags) : this(move, NormalDefense.INSTANCE, special, null, tags) {}
	}


	/// Qualities that very few pieces have.
	public enum PTTag {
		/// Piece can be checked; you lose if this piece is captured.
		Essential,
		/// If this piece reaches the other side of the board, it promotes to a piece of your choice.
		Promotes,
		/// When this piece captures another, it immediately moves back to its old position.
		Ranged,
	}

}