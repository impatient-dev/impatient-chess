﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using static imp.chess.game.PostMoveUtil;


namespace imp.chess.game {

	/// Represents a thing a player can do. Normally each player makes 1 move per turn, though there are exceptions.
 	public interface Move {
		/// Moves and captures pieces on the board. Returns a non-null PostMove if there is some action you need to take afterward.
		/// This function should not update the en passant column - we need to do that elsewhere so the 2nd move in a 2-move turn won't wipe out the value from the 1st move.
		void Apply(Board board, [CanBeNull] out PostMove postMove);
		/// What the board's EnPassantCol should be set to for this move: -1 unless this move creates a vulnerability to en passant capture.
		int EnPassantCol { get; }
	}

	
	/// Moves a single piece from 1 location to another, capturing any piece located at the destination.
	public readonly struct NormalMove : Move {
		public readonly Vector2Int From, To;
		public int EnPassantCol => -1;

		public NormalMove(Vector2Int from, Vector2Int to) {
			From = from;
			To = to;
		}
		
		public void Apply(Board board, out PostMove postMove) {
			postMove = ForPossibleCapture(board, To);
			if(board.At(From).Type.Ranged && board.At(To).Exists)
				board.At(To) = Piece.NONE;
			else {
				board.Move(From, To);
				if (postMove == null)
					postMove = ForMove(board, To);
			}
		}

		public override string ToString() => $"Move({From.SquareName()}-{To.SquareName()})";
	}
	

	/// A move that falls outside what you're able to do under the Movement interface.
	public class SpecialMove : Move {
		/// The piece making this move.
		public readonly Vector2Int Pos;
		/// A user-friendly name for this move, such as "Castle".
		public readonly string Name;
		/// Pieces that are captured by this move, without any other piece moving to take their place.
		/// If a piece is captured by some other piece moving to the same square, that should be noted in the moves list, not the captures list.
		public readonly IReadOnlyList<Vector2Int> Captures;
		/// Pieces that change position due to this move, to an empty or occupied square. If this is empty, no pieces change position, including the piece making this "move".
		public readonly IReadOnlyList<NormalMove> Moves;
		/// What the board's EnPassantCol value should be set to - normally -1, but something else if this is a pawn double-move.
		public int EnPassantCol { get; }

		public SpecialMove(Vector2Int pos, string name, IReadOnlyList<Vector2Int> captures, IReadOnlyList<NormalMove> moves, int enPassantCol = -1) {
			Pos = pos;
			Name = name;
			Captures = captures;
			Moves = moves;
			EnPassantCol = enPassantCol;
		}

		public void Apply(Board board, out PostMove postMove) {
			postMove = null;
			
			foreach (var capture in Captures) {
				board.At(capture) = Piece.NONE;
				if (postMove == null)
					postMove = ForCapture(board, capture);
			}
			
			foreach (var move in Moves) {
				board.Move(move.From, move.To);
				if (postMove == null)
					postMove = ForMove(board, move.To);
			}
		}
		
		public override string ToString() => $"SpecialMove({Pos.SquareName()} {Name}: {Moves.Count} moves + {Captures.Count} captures; EP={EnPassantCol})";
	}

	
	public abstract class SpecialMoveFinder {
		/// How many special moves we expect to find; used to allocate a list of the correct size;
		public readonly int ExpectedCount;

		protected SpecialMoveFinder(int expectedCount) {
			ExpectedCount = expectedCount;
		}

		/// Finds all special moves that are possible, ignoring whether they put or leave the king in check.
		public abstract IEnumerable<SpecialMove> Find(Board board, Vector2Int from);

		public IReadOnlyList<SpecialMove> List(Board board, Vector2Int from) {
			if (ExpectedCount == 0)
				return Empty.SpecialMove;
			var ret = new List<SpecialMove>(ExpectedCount);
			foreach(var special in Find(board, from))
				ret.Add(special);
			return ret;
		}
	}
	

	public class NoSpecialMoves : SpecialMoveFinder {
		public static readonly NoSpecialMoves INSTANCE = new NoSpecialMoves();
		private NoSpecialMoves() : base(0) {}
		public override IEnumerable<SpecialMove> Find(Board board, Vector2Int from) {
			yield break;
		}
	}

}