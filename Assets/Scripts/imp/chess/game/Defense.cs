﻿using System;
using UnityEngine;


namespace imp.chess.game {

	/// Defines what is allowed to capture this piece, based on the position and type of the attacker.
	public interface Defense {
		/// Returns false if the position or type of the attacker forbids him from attacking us.
		/// The attacker must exist.
		bool IsVulnerable(Board board, Vector2Int attacker, Vector2Int defender);
	}


	/// Can be captured by anything.
	public class NormalDefense : Defense {
		public static readonly NormalDefense INSTANCE = new NormalDefense();
		public bool IsVulnerable(Board board, Vector2Int attacker, Vector2Int defender) => true;
	}

	/// Cannot be captured.
	public class InvulnerableDefense : Defense {
		public static readonly InvulnerableDefense INSTANCE = new InvulnerableDefense();
		public bool IsVulnerable(Board board, Vector2Int attacker, Vector2Int defender) => false;
	}

	/// Can only be captured by units in a square-shaped region around itself.
	public class FarDefense : Defense {
		public readonly int Radius;
		public FarDefense(int radius) {
			Radius = radius;
		}
		public bool IsVulnerable(Board board, Vector2Int attacker, Vector2Int defender) => Math.Abs(defender.x - attacker.x) <= Radius && Math.Abs(defender.y - attacker.y) <= Radius;
	}

}