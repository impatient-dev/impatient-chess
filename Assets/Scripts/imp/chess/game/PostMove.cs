using System;
using JetBrains.Annotations;
using UnityEngine;

namespace imp.chess.game {
	/// Marker interface for special situations that a move can cause.
	public interface PostMove {}


	/// Someone lost the game because their king was captured.
	public class PostMoveLoss : PostMove {
		/// The location of the captured king.
		public readonly Vector2Int Pos;
		/// Whether the losing side is white.
		public readonly bool WhiteVictim;

		public PostMoveLoss(Vector2Int pos, bool whiteVictim) {
			Pos = pos;
			WhiteVictim = whiteVictim;
		}

		public override string ToString() => $"Loss({WhiteVictim.WhiteStr()} @ {Pos.SquareName()})";
	}


	/// Someone won the game because their king crossed the midline.
	public class PostMoveMidlineInvasion : PostMove {
		/// The location of the king that moved.
		public readonly Vector2Int Pos;
		/// Whether the winning side is white.
		public readonly bool WhiteVictor;

		public PostMoveMidlineInvasion(Vector2Int pos, bool whiteVictor) {
			Pos = pos;
			WhiteVictor = whiteVictor;
		}

		public override string ToString() => $"MidlineInvasion({WhiteVictor.WhiteStr()} @ {Pos.SquareName()})";
	}


	/// A pawn reached the back rank and needs to be promoted.
	public class PostMovePromotion : PostMove {
		/// The pawn that needs to be promoted.
		public readonly Vector2Int Pos;
		/// Whether the pawn is white.
		public readonly bool White;

		public PostMovePromotion(Vector2Int pos, bool white) {
			Pos = pos;
			White = white;
		}

		public override string ToString() => $"Promotion({White.WhiteStr()} @ {Pos.SquareName()}";
	}



	public static class PostMoveUtil {
		/// Determines if anything needs to happen post-move when the piece at the provided location is captured.
		/// The piece may or may not exist.
		[CanBeNull] public static PostMove ForPossibleCapture(Board board, Vector2Int pos) {
			var piece = board.At(pos);
			if(piece.Exists && piece.Type.Essential)
				return new PostMoveLoss(pos, piece.White);
			return null;
		}
		
		/// Determines if anything needs to happen post-move when the piece at the provided location is captured.
		/// The piece must exist.
		[CanBeNull] public static PostMove ForCapture(Board board, Vector2Int pos) {
			var piece = board.At(pos);
			if(piece.Exists && piece.Type.Essential)
				return new PostMoveLoss(pos, piece.White);
			return null;
		}

		/// Determines whether anything needs to happen post-move after a piece moves to a location.
		/// The piece must already be present at the destination; any capture by this move is not considered.
		[CanBeNull] public static PostMove ForMove(Board board, Vector2Int to) {
			var piece = board.At(to);
			if (piece.Type.Essential) {
				if (board.Rules.MidlineInvasion && IsAcrossMidline(board, to, piece.White) && AllKingsAcrossMidline(board, piece.White))
					return new PostMoveMidlineInvasion(to, piece.White);
			} else if (piece.Type.Promotes) {
				if(to.Row() == PromotionRow(board, piece.White))
					return new PostMovePromotion(to, piece.White);
			}
			return null;
		}


		/// The row a side needs to invade to win by midline invasion.
		private static int MidlineInvasionRow(Board board, bool white) => board.Rows / 2 + (white ? 0 : -1);

		private static bool IsAcrossMidline(Board board, Vector2Int pos, bool white) => white ? pos.Row() >= MidlineInvasionRow(board, true) : pos.Row() <= MidlineInvasionRow(board, false);

		private static bool AllKingsAcrossMidline(Board board, bool white) {
			foreach (var pos in board.Positions()) {
				var piece = board.At(pos);
				if (piece.Exists && piece.Type.Essential && piece.White == white && !IsAcrossMidline(board, pos, white))
					return false;
			}
			return true;
		}
		
		private static int PromotionRow(Board board, bool white) => white ? board.RowMax : 0;
	}
}