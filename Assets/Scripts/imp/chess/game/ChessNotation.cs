﻿using UnityEngine;


namespace imp.chess.game {

	public static class ChessNotation {
		public static string RowName(int row) => (row + 1).ToString();
		public static string ColName(int col) => ((char)('a' + col)).ToString();
		public static string SquareName(this Vector2Int pos) => $"{RowName(pos.x)}{ColName(pos.y)}";
		/// Returns "White" or "Black".
		public static string WhiteStr(this bool white) => white ? "White" : "Black";
	}

}