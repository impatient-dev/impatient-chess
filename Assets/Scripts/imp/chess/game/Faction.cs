﻿namespace imp.chess.game {

	public class Faction {
		public readonly string Name;
		public readonly PieceType Pawn, King, Queen, Bishop, Knight, Rook;
		public readonly PieceType[] PromotionOptions;

		public Faction(string name, PieceType king, PieceType queen, PieceType bishop, PieceType knight, PieceType rook, PieceType pawn) {
			Name = name;
			King = king;
			Queen = queen;
			Bishop = bishop;
			Knight = knight;
			Rook = rook;
			Pawn = pawn;
			PromotionOptions = new[] { queen.TypePostMove, bishop.TypePostMove, knight.TypePostMove, rook.TypePostMove };
		}
	}

}