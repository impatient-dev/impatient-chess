﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;


namespace imp.chess.game {

	/// Defines which empty and occupied squares a piece is allowed to move to. For attacks, the target's Defense is considered.
	/// This interface only covers moves where 1 piece moves from 1 square to another, capturing any piece at the destination;
	/// other moves, such as castling and en passant, are not covered.
	/// Most implementations of this interface are only able to attack enemy pieces, not friends.
	public interface Movement {
		/// Finds all valid moves, ignoring whether they put or leave the king in check.
		IEnumerable<Vector2Int> Find(Board board, Vector2Int from);
		/// Finds all valid moves, ignoring whether they put or leave the king in check.
		IReadOnlyList<Vector2Int> List(Board board, Vector2Int from);
	}


	/// A movement class that generates moves via yield.
	public abstract class YieldMovement : Movement {
		public abstract IEnumerable<Vector2Int> Find(Board board, Vector2Int from);
		/// How many moves we expect to produce, at maximum. Used to allocate a list of the right size.
		public readonly int ExpectedCount;

		protected YieldMovement(int expectedCount) {
			ExpectedCount = expectedCount;
		}

		public IReadOnlyList<Vector2Int> List(Board board, Vector2Int from) {
			var ret = new List<Vector2Int>(ExpectedCount);
			foreach(var move in Find(board, from))
				ret.Add(move);
			return ret;
		}
	}


	/// A piece that moves orthogonally in any direction.
	public class OrthoMovement : YieldMovement {
		public static readonly OrthoMovement UNLIMITED = new OrthoMovement(int.MaxValue);
		public static readonly OrthoMovement ONE = new OrthoMovement(1);
		
		/// Max distance; MaxValue if unlimited.*/
		public readonly int Limit;

		public OrthoMovement(int limit) : base( 4 * Math.Min(4, limit)) {
			Limit = limit;
		}

		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			var white = board.At(from).White;
			foreach (var dir in Dirs.ORTHOGONAL) {
				for (var d = 1; d <= Limit; d++) {
					var tgt = from + dir * d;
					if (!board.Contains(tgt))
						break;
					var target = board.At(tgt);
					if (target.Exists) {
						if (target.White != white && target.Type.Defense.IsVulnerable(board, from, tgt))
							yield return tgt;
						break;
					}
					yield return tgt;
				}
			}
		}
	}


	/// A piece that moves diagonally in any direction.
	public class DiagMovement : YieldMovement {
		public static readonly DiagMovement UNLIMITED = new DiagMovement(int.MaxValue);
		public static readonly DiagMovement ONE = new DiagMovement(1);
		
		/// Max distance; MaxValue if unlimited.*/
		public readonly int Limit;

		public DiagMovement(int limit) : base(4 * Math.Min(4, limit)) {
			Limit = limit;
		}

		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			var white = board.At(from).White;
			foreach (var dir in Dirs.DIAGONAL) {
				for (var d = 1; d <= Limit; d++) {
					var tgt = from + dir * d;
					if (!board.Contains(tgt))
						break;
					var target = board.At(tgt);
					if (target.Exists) {
						if (target.White != white && target.Type.Defense.IsVulnerable(board, from, tgt))
							yield return tgt;
						break;
					}
					yield return tgt;
				}
			}
		}
	}


	/// Moves in an L shape, and jumps over all intervening pieces.
	public class LMovement : YieldMovement {
		/// The 2 legs of the jump.
		public readonly int Long, Short;
		private Vector2Int[] Offsets;

		public LMovement(int lng, int shrt) : base(8) {
			Long = lng;
			Short = shrt;
			if(Short > Long)
				throw new Exception($"{Short} > {Long}.");
			Offsets = new[] {
				V2I(Long, Short), V2I(-Long, Short), V2I(-Long, -Short), V2I(Long, -Short),
				V2I(Short, Long), V2I(-Short, Long), V2I(-Short, -Long), V2I(Short, -Long),
			};
		}

		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			var ret = new List<Vector2Int>(8);
			var white = board.At(from).White;
			foreach (var offset in Offsets) {
				var tgt = from + offset;
				if (!board.Contains(tgt))
					continue;
				var piece = board.At(tgt);
				if (!piece.Exists || (piece.White != white && piece.Type.Defense.IsVulnerable(board, from, tgt)))
					yield return tgt;
			}
		}
	}
	

	/// Combines multiple movement types. Try to avoid adding multiple types that can return the same moves - it's inefficient.
	public class ComboMovement : YieldMovement {
		public readonly YieldMovement[] Delegates;

		public ComboMovement(params YieldMovement[] delegates) : base(delegates.Sum(d => d.ExpectedCount)) {
			Delegates = delegates;
		}

		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			foreach(var d in Delegates)
				foreach(var tgt in d.Find(board, from))
					yield return tgt;
		}
	}


	/// Can move to any unoccupied square.
	public class UnoccupiedTeleport : YieldMovement {
		public static readonly UnoccupiedTeleport INSTANCE = new UnoccupiedTeleport();
		private UnoccupiedTeleport() : base(8 * 8) {}

		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			foreach(var tgt in board.Positions())
				if (!board.At(tgt).Exists)
					yield return tgt;
		}
	}

}