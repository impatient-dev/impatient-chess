﻿using UnityEngine;


namespace imp.chess.game {

	/// Directions on the board. North is toward the black home row; east is toward the "h" column.
	public static class Dirs {
		public static readonly Vector2Int N = new Vector2Int(1, 0);
		public static readonly Vector2Int S = new Vector2Int(-1, 0);
		public static readonly Vector2Int E = new Vector2Int(0, 1);
		public static readonly Vector2Int W = new Vector2Int(0, -1);
		
		public static readonly Vector2Int NE = N + E;
		public static readonly Vector2Int NW = N + W;
		public static readonly Vector2Int SE = S + E;
		public static readonly Vector2Int SW = S + W;

		public static readonly Vector2Int[] ORTHOGONAL = { N, S, E, W };
		public static readonly Vector2Int[] DIAGONAL = { NE, NW, SE, SW };
		public static readonly Vector2Int[] VERTICAL = { N, S };
		public static readonly Vector2Int[] HORIZONTAL = { E, W };
		public static readonly Vector2Int[] ALL = { N, S, E, W, NE, NW, SE, SW };
	}

}