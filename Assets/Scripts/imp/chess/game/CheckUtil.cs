using UnityEngine;

namespace imp.chess.game {
	/// Util class for calculating when players are in check.
	public static class CheckUtil {
		/// Returns true if the other side could capture a king after you make this move.
		public static bool MovesIntoCheck(Board initialBoard, Vector2Int piecePos, Move move) {
			return IsInCheck(initialBoard.With(move), initialBoard.At(piecePos).White);
		}
		
		
		/// Returns whether any of the specified color's kings could be taken by an opponent in 1 move.
		public static bool IsInCheck(Board board, bool white) {
			foreach(var pos in board.Positions()) {
				var piece = board.At(pos);
				if (!piece.Exists || piece.White == white)
					continue;
				foreach(var tgt in piece.Type.Move.Find(board, pos))
					if (IsKing(board, white, tgt))
						return true;
				foreach (var special in piece.Type.Special.Find(board, pos)) {
					foreach(var capture in special.Captures)
						if (IsKing(board, white, capture))
							return true;
					foreach (var m in special.Moves)
						if(IsKing(board, white, m.To))
							return true;
				}
			}

			return false;
		}


		private static bool IsKing(Board board, bool white, Vector2Int pos) {
			var piece = board.At(pos);
			if (!piece.Exists || white != piece.White)
				return false;
			if (piece.Type.Essential)
				return true;
			return false;
		}


		/// Returns whether the specified side has any valid moves available, excluding moves that leave them in check.
		/// If this returns false and it's their turn, they are stalemated or in checkmate.
		public static bool HasAnyValidMove(Board board, bool white) {
			foreach (var pos in board.Positions()) {
				var piece = board.At(pos);
				if (!piece.Exists || piece.White != white)
					continue;
				foreach(var tgt in piece.Type.Move.Find(board, pos))
					if (!MovesIntoCheck(board, pos, new NormalMove(pos, tgt)))
						return true;
				foreach(var move in piece.Type.Special.Find(board, pos))
					if (!MovesIntoCheck(board, pos, move))
						return true;
			}
			
			return false;
		}
	}
}