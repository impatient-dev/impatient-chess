﻿using System.Collections.Generic;
using System.Linq;
using imp.util;
using UnityEngine;
using static imp.chess.game.CheckUtil;


namespace imp.chess.game {

	/// Calculates all possible moves that can be made on the board on a turn. Finds and excludes moves that would move into check. Immutable.
	public class Possibilities {
		private readonly Board Board;
		private readonly IReadOnlyList<Vector2Int>[,] _movesFrom;
		private readonly IReadOnlyList<Vector2Int>[,] _movesTo;
		private readonly IReadOnlyList<SpecialMove>[,] _specialsFrom;
		private readonly IReadOnlyList<SpecialMove>[,] _specialsTo;


		public Possibilities(Board board, Turn turn) {
			Board = board;
			_movesFrom = new IReadOnlyList<Vector2Int>[board.Rows, board.Cols];
			_movesTo = new IReadOnlyList<Vector2Int>[board.Rows, board.Cols];
			_specialsFrom = new IReadOnlyList<SpecialMove>[board.Rows, board.Cols];
			_specialsTo = new IReadOnlyList<SpecialMove>[board.Rows, board.Cols];
			
			foreach(var pos in board.Positions()) {
				var piece = board.At(pos);
				if (!piece.Exists || !turn.CanMove(piece)) {
					_movesFrom[pos.Row(), pos.Col()] = Empty.Pos;
					_specialsFrom[pos.Row(), pos.Col()] = Empty.SpecialMove;
					return;
				}

				var moves = new List<Vector2Int>();
				foreach(var target in piece.Type.Move.Find(board, pos))
					if (!MovesIntoCheck(board, pos, new NormalMove(pos, target))) {
						moves.Add(target);
						AddMoveTo(pos, target);
					}
				_movesFrom[pos.Row(), pos.Col()] = moves.Count == 0 ? Empty.Pos : moves;

				var specials = new List<SpecialMove>();
				foreach(var special in piece.Type.Special.Find(board, pos))
					if (!MovesIntoCheck(board, pos, special)) {
						specials.Add(special);
						foreach (var c in special.Captures)
							AddSpecialTo(special, c);
						foreach (var m in special.Moves)
							AddSpecialTo(special, m.To);
					}
				_specialsFrom[pos.Row(), pos.Col()] = specials.Count == 0 ? Empty.SpecialMove : specials;
			};
			
			foreach(var pos in board.Positions()) {
				if (_movesTo[pos.Row(), pos.Col()] == null)
					_movesTo[pos.Row(), pos.Col()] = Empty.Pos;
				if (_specialsTo[pos.Row(), pos.Col()] == null)
					_specialsTo[pos.Row(), pos.Col()] = Empty.SpecialMove;
			};
		}


		private void AddMoveTo(Vector2Int from, Vector2Int to) {
			var list = _movesTo[to.Row(), to.Col()];
			if(list != null && list.Count > 0) //don't pollute one of the shared EMPTY lists
				((List<Vector2Int>)list).Add(from);
			else
				_movesTo[to.Row(), to.Col()] = new List<Vector2Int>().Also(x => x.Add(from));
		}
		
		private void AddSpecialTo(SpecialMove special, Vector2Int to) {
			var list = _specialsTo[to.Row(), to.Col()];
			if(list != null && list.Count > 0) //don't pollute one of the shared EMPTY lists
				((List<SpecialMove>)list).Add(special);
			else
				_specialsTo[to.Row(), to.Col()] = new List<SpecialMove>().Also(x => x.Add(special));
		}


		/// Returns all the squares this square can move to, excluding special moves.
		public IReadOnlyList<Vector2Int> MovesFrom(Vector2Int pos) => _movesFrom[pos.Row(), pos.Col()];
		/// Returns all the squares that can move to this square, excluding special moves.
		public IReadOnlyList<Vector2Int> MovesTo(Vector2Int pos) => _movesTo[pos.Row(), pos.Col()];
		/// Returns all the special moves a piece can make.
		public IReadOnlyList<SpecialMove> SpecialsFrom(Vector2Int pos) => _specialsFrom[pos.Row(), pos.Col()];
		/// Returns all the special moves that can either capture or move TO the provided square.
		public IReadOnlyList<SpecialMove> SpecialsTo(Vector2Int pos) => _specialsTo[pos.Row(), pos.Col()];

		/// Can handle invalid coordinates. Examines normal moves only.
		public bool CanMove(Vector2Int from, Vector2Int to) {
			if (!Board.Contains(to))
				return false;
			return MovesTo(to).Contains(from);
		}
	}

}