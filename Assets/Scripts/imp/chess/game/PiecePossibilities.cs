﻿using System.Collections.Generic;
using imp.util;
using UnityEngine;
using static imp.chess.game.CheckUtil;


namespace imp.chess.game {

	/// Calculates all possible moves a piece could make on a turn, for use by the UI. Immutable. Avoids moving into check.
	public class PiecePossibilities {
		public static readonly PiecePossibilities Empty = new PiecePossibilities(
			new HashSet<Vector2Int>(), new Dictionary<Vector2Int, List<SpecialMove>>(), new Dictionary<Vector2Int, List<SpecialMove>>());
		
		private readonly HashSet<Vector2Int> NormalMoves;
		private readonly Dictionary<Vector2Int, List<SpecialMove>> SpecialMovesBySource;
		private readonly Dictionary<Vector2Int, List<SpecialMove>> SpecialMovesByTarget;

		private PiecePossibilities(HashSet<Vector2Int> normalMoves, Dictionary<Vector2Int, List<SpecialMove>> specialMovesBySource, Dictionary<Vector2Int, List<SpecialMove>> specialMovesByTarget) {
			NormalMoves = normalMoves;
			SpecialMovesBySource = specialMovesBySource;
			SpecialMovesByTarget = specialMovesByTarget;
		}

		public static PiecePossibilities Calculate(Board board, Turn turn, Vector2Int pos) {
			if (!board.Contains(pos))
				return Empty;
			var piece = board.At(pos);
			if (!piece.Exists || !turn.CanMove(piece))
				return Empty;

			var normalMoves = new HashSet<Vector2Int>();
			var specialMovesBySource = new Dictionary<Vector2Int, List<SpecialMove>>();
			var specialMovesByTarget = new Dictionary<Vector2Int, List<SpecialMove>>();
			
			foreach(var tgt in piece.Type.Move.Find(board, pos))
				if (!MovesIntoCheck(board, pos, new NormalMove(pos, tgt)))
					normalMoves.Add(tgt);
			foreach(var special in piece.Type.Special.Find(board, pos))
				if (!MovesIntoCheck(board, pos, special)) {
					specialMovesBySource.GetOrCreate(special.Pos, () => new List<SpecialMove>(4)).Add(special);
					foreach (var c in special.Captures)
						specialMovesByTarget.GetOrCreate(c, () => new List<SpecialMove>()).Add(special);
					foreach(var m in special.Moves)
						if (m.From == pos)
							specialMovesByTarget.GetOrCreate(m.To, () => new List<SpecialMove>()).Add(special);
				}
			
			return new PiecePossibilities(normalMoves, specialMovesBySource, specialMovesByTarget);
		}


		/// Only considers normal moves.
		public bool CanMoveTo(Vector2Int pos) => NormalMoves.Contains(pos);

		/// Returns all special moves this piece can make
		public IReadOnlyList<SpecialMove> SpecialsFrom(Vector2Int pos) => SpecialMovesBySource.Opt(pos) ?? imp.chess.Empty.SpecialMove;

		/// Returns all special moves that either capture this position, or move the original piece to this position.
		public IReadOnlyList<SpecialMove> SpecialsTargeting(Vector2Int pos) => SpecialMovesByTarget.Opt(pos) ?? imp.chess.Empty.SpecialMove;
	}

}