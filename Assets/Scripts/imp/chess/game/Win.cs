namespace imp.chess.game {
	
	public enum WinReason {
		/// One player checkmated the other.
		Checkmate,
		/// The player whose turn it is has no valid moves.
		Stalemate,
		/// One player has captured the other's king.
		Capture,
		/// One player got his king(s) across the midline.
		MidlineInvasion,
	}


	public struct GameWinner {
		public readonly bool White;
		public readonly WinReason Reason;

		public GameWinner(bool white, WinReason reason) {
			White = white;
			Reason = reason;
		}
	}
}