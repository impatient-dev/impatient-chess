﻿using System;
using System.Collections.Generic;
using imp.util;
using UnityEngine;
using static imp.chess.game.ClassicFaction;
using static imp.chess.ChessUtil;
using static imp.util.ImpatientUnityUtil;

namespace imp.chess.game {

	public static class Chess2 {
		public static readonly PieceType NemesisQueen = new PieceType(
			NemesisQueenMove.INSTANCE, KingOnlyDefense.INSTANCE, NoSpecialMoves.INSTANCE, null);
		public static readonly PieceType NemesisPawn = new PieceType(NemesisPawnMove.INSTANCE, NoSpecialMoves.INSTANCE, PTTag.Promotes);
		public static readonly Faction Nemesis = new Faction("Nemesis", King, NemesisQueen, Bishop, Knight, Rook, NemesisPawn);
		
		public static readonly PieceType EmpoweredBishop = new PieceType(new EmpoweredMovement(DiagMovement.UNLIMITED), NoSpecialMoves.INSTANCE);
		public static readonly PieceType EmpoweredKnight = new PieceType(new EmpoweredMovement((YieldMovement)Knight.Move), NoSpecialMoves.INSTANCE);
		public static readonly PieceType EmpoweredRook = new PieceType(new EmpoweredMovement(OrthoMovement.UNLIMITED), NoSpecialMoves.INSTANCE);
		public static readonly PieceType ElegantQueen = new PieceType(King.Move, NoSpecialMoves.INSTANCE);
		public static readonly Faction Empowered = new Faction("Empowered", King, ElegantQueen, EmpoweredBishop, EmpoweredKnight, EmpoweredRook, UnmovedPawn);
		
		public static readonly PieceType WarriorKing = new PieceType(King.Move, WarriorKingSpecial.INSTANCE, PTTag.Essential);
		public static readonly Faction TwoKings = new Faction("Two Kings", WarriorKing, WarriorKing, Bishop, Knight, Rook, UnmovedPawn);
		
		public static readonly PieceType ReaperQueen = new PieceType(ReaperQueenMove.INSTANCE, NoSpecialMoves.INSTANCE);
		public static readonly PieceType ReaperGhost = new PieceType(UnoccupiedTeleport.INSTANCE, InvulnerableDefense.INSTANCE, NoSpecialMoves.INSTANCE, null);
		public static readonly Faction Reaper = new Faction("Reaper", King, ReaperQueen, Bishop, Knight, ReaperGhost, UnmovedPawn);
		
		public static readonly PieceType JungleQueen = new PieceType(new ComboMovement(OrthoMovement.ONE, (YieldMovement)Knight.Move), NoSpecialMoves.INSTANCE);
		public static readonly PieceType Tiger = new PieceType(new DiagMovement(2), NoSpecialMoves.INSTANCE, PTTag.Ranged);
		public static readonly PieceType WildHorse = new PieceType(WildHorseMove.INSTANCE, NoSpecialMoves.INSTANCE);
		public static readonly PieceType Elephant = new PieceType(ElephantMove.INSTANCE, new FarDefense(2), ElephantSpecial.INSTANCE, null);
		public static readonly Faction Animals = new Faction("Animals", King, JungleQueen, Tiger, WildHorse, Elephant, UnmovedPawn);

		public static readonly Faction[] Factions = {Nemesis, Empowered, TwoKings, Reaper, Animals};
	}

	
	

	public class NemesisQueenMove : YieldMovement {
		public static readonly NemesisQueenMove INSTANCE = new NemesisQueenMove();
		private NemesisQueenMove() : base(8 * 4) {}

		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			foreach (var tgt in Queen.Move.Find(board, from)) {
				var piece = board.At(tgt);
				if (!piece.Exists || piece.Type.Essential) //queen move already checks color
					yield return tgt;
			}
		}
	}

	public class KingOnlyDefense : Defense {
		public static readonly KingOnlyDefense INSTANCE = new KingOnlyDefense();
		public bool IsVulnerable(Board board, Vector2Int attacker, Vector2Int defender) => board.At(attacker).Type.Essential;
	}


	public class NemesisPawnMove : YieldMovement {
		public static readonly NemesisPawnMove INSTANCE = new NemesisPawnMove();
		
		public NemesisPawnMove() : base(8) { }
		
		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			var white = board.At(from).White;
			var fwd = Forward(white);
			
			//normal pawn movement
			if(!board.At(from + fwd).Exists)
				yield return from + fwd;
			foreach (var side in Dirs.HORIZONTAL) {
				var tgt = from + fwd + side;
				if(board.Contains(tgt) && board.At(tgt).Exists && board.At(tgt).White != white && board.At(tgt).Type.Defense.IsVulnerable(board, from, tgt))
					yield return tgt;
			}
			
			//TODO it's inefficient to have each pawn calculate this separately
			var kingPositions = new List<Vector2Int>(2);
			foreach (var pos in board.Positions()) {
				var piece = board.At(pos);
				if(piece.Exists && piece.Type.Essential && piece.White != white)
					kingPositions.Add(pos);
			}

			//extra nemesis moves
			foreach (var dir in Dirs.ALL) {
				if (dir == fwd)
					continue;
				var tgt = from + dir;
				if (!board.Contains(tgt) || board.At(tgt).Exists)
					continue;
				foreach (var kingPos in board.Positions()) {
					var curDiff = (from - kingPos).Abs();
					var newDiff = (tgt - kingPos).Abs();
					if (curDiff.x >= newDiff.x && curDiff.y >= newDiff.y)
						yield return tgt;
				}
			}
		}
	}


	/// Has an inherent movement, and also uses the movement of any adjacent friendly pieces that also use EmpoweredMovement.
	public class EmpoweredMovement : YieldMovement {
		public readonly Movement Inherent;

		public EmpoweredMovement(YieldMovement inherent) : base(inherent.ExpectedCount) {
			Inherent = inherent;
		}

		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			foreach (var tgt in Inherent.Find(board, from))
				yield return tgt;
			
			var white = board.At(from).White;
			foreach (var dir in Dirs.ORTHOGONAL) {
				var pos = from + dir;
				if (!board.Contains(pos))
					continue;
				var piece = board.At(pos);
				if (piece.Exists && piece.White == white && piece.Type.Move is EmpoweredMovement emp && Inherent != emp.Inherent)
					foreach (var tgt in emp.Inherent.Find(board, from))
						yield return tgt;
			}
		}
	}
	

	public class WarriorKingSpecial : SpecialMoveFinder {
		public static readonly WarriorKingSpecial INSTANCE = new WarriorKingSpecial();
		private WarriorKingSpecial() : base(1) {}

		public override IEnumerable<SpecialMove> Find(Board board, Vector2Int from) {
			var captures = new List<Vector2Int>(8);
			foreach (var dir in Dirs.ALL) {
				var target = from + dir;
				if (!board.Contains(target))
					continue;
				var piece = board.At(target);
				if (piece.Exists) {
					if(piece.Type.Essential && piece.White == board.At(from).White) //can't capture your own king
						yield break;
					captures.Add(target);
				}
			}
			yield return new SpecialMove(from, "Whirlwind Attack", captures, Empty.NormalMove);
		}
	}


	public class ReaperQueenMove : YieldMovement {
		public static readonly ReaperQueenMove INSTANCE = new ReaperQueenMove();
		private ReaperQueenMove() : base(8 * 7) {}
		
		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			var white = board.At(from).White;
			var r0 = white ? 0 : 1;
			
			for(var row = r0; row < r0 + board.Rows - 1; row++)
				for (var col = 0; col < board.Cols; col++) {
					var tgt = new Vector2Int(row, col);
					var piece = board.At(tgt);
					if (!piece.Exists || (piece.White != white && !piece.Type.Essential && piece.Type.Defense.IsVulnerable(board, from, tgt)))
						yield return tgt;
				}
		}
	}
	
	
	/// Moves in an L shape, and jumps over all intervening pieces.
	public class WildHorseMove : YieldMovement {
		public static readonly WildHorseMove INSTANCE = new WildHorseMove();
		private WildHorseMove() : base(8) {}

		private readonly Vector2Int[] Offsets = {
			V2I(2, 1), V2I(-2, 1), V2I(-2, -1), V2I(2, -1),
			V2I(1, 2), V2I(-1, 2), V2I(-1, -2), V2I(1, -2)
		};

		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			var white = board.At(from).White;
			foreach (var offset in Offsets) {
				var tgt = from + offset;
				if (!board.Contains(tgt))
					continue;
				var piece = board.At(tgt);
				if (!piece.Exists || (piece.Type.Defense.IsVulnerable(board, from, tgt) && (!piece.Type.Essential || piece.White != board.At(from).White)))
					yield return tgt;
			}
		}
	}


	/// Moves up to 3 spaces, but can only capture pieces exactly 3 spaces away.
	public class ElephantMove : YieldMovement {
		public static readonly ElephantMove INSTANCE = new ElephantMove();
		private ElephantMove() : base(3 * 4) {}
		
		public override IEnumerable<Vector2Int> Find(Board board, Vector2Int from) {
			foreach (var dir in Dirs.ORTHOGONAL) {
				for (var d = 1; d <= 3; d++) {
					var tgt = from + dir * d;
					var piece = board.At(tgt);
					if(!piece.Exists)
						yield return tgt;
					else {
						if(d == 3 && piece.Type.Defense.IsVulnerable(board, from, tgt) && (!piece.Type.Essential || piece.White != board.At(from).White))
							yield return tgt;
						break;
					}
				}
			}
		}
	}


	/// Captures pieces 1 or 2 squares away, then moves the full 3 squares.
	/// Stops the move early if an invincible piece is encountered.
	public class ElephantSpecial : SpecialMoveFinder {
		public static readonly ElephantSpecial INSTANCE = new ElephantSpecial();
		private ElephantSpecial() : base(4) {}

		public override IEnumerable<SpecialMove> Find(Board board, Vector2Int from) {
			foreach (var dir in Dirs.ORTHOGONAL) {
				for (var d = 1; d <= 2; d++) { // d==3 is covered by ElephantMove
					var tgt = from + dir * d;
					if (!board.Contains(tgt))
						break;
					var piece = board.At(tgt);
					if (!piece.Exists)
						continue;
					if (!piece.Type.Defense.IsVulnerable(board, from, tgt))
						break;
					
					//we've confirmed this attack is possible; find all pieces we capture, and our final resting position
					var captures = new List<Vector2Int>(3);
					captures.Add(from);
					var finalPos = from;
					while (++d <= 3) {
						tgt = from + dir * d;
						if (!board.Contains(tgt))
							break;
						piece = board.At(tgt);
						if (!piece.Exists)
							finalPos = tgt;
						else if (piece.Type.Defense.IsVulnerable(board, from, tgt)) {
							captures.Add(tgt);
							finalPos = tgt;
						} else
							break;
					}
					
					var moveList = new List<NormalMove>(1).Also(list => list.Add(new NormalMove(from, finalPos)));
					yield return new SpecialMove(from, "Elephant attack", captures, moveList);
					break;
				}
			}
		}
	}
	
}