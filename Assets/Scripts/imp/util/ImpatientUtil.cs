﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;


namespace imp.util {

	/**General C# utilities unrelated to Unity.*/
	public static class ImpatientUtil {
	
		// GENERAL

		/**Returns the object, but calls your lambda on it first.*/
		public static T Also<T>(this T obj, [NotNull] Action<T> action) {
			action.Invoke(obj);
			return obj;
		}

		public static void Swap<T>(ref T a, ref T b) {
			var temp = a;
			a = b;
			b = temp;
		}
		
		/**Throws an exception. Put this where you plan to write code later.*/
		public static void TODO() => throw new NotImplementedException("This code has not been written yet.");
		/**Throws an exception because we reached some code it should not be possible to reach.*/
		public static void Unreachable() => throw new Exception("It should not be possible to reach this code.");
		/**Throws an exception because we encountered a type we do not know how to handle.*/
		public static void UnexpectedType([CanBeNull] object thing) => throw new Exception("Unexpected type: " + thing?.GetType());



		// ARRAYS

		/**Returns the last item in the array. The array must not be empty.*/
		public static ref T Last<T>(this T[] array) => ref array[array.Length - 1];
		/**Returns the first index if false, or the last index if true. The array must not be empty.*/
		public static int EndIdx<T>(this T[] array, bool last) => last ? array.Length - 1 : 0;
		/**Returns the first item if false, or last if true. The array must not be empty.*/
		public static ref T End<T>(this T[] array, bool last) => ref array[last ? array.Length - 1 : 0];

		/**Finds and returns the index of the value, or else throws an exception.*/
		public static int RequireIndex<T>(this T[] array, T value) {
			for(var c = 0; c < array.Length; c++)
				if (Equals(array[c], value))
					return c;
			throw new Exception($"Value not found in array (len={array.Length}): {value}");
		}

		/**Returns whether this is a valid index in the array.*/
		public static bool Valid<T>(this T[] array, int index) => index >= 0 && index < array.Length;
		
		
		
		// LISTS

		public static List<T> ListOf<T>(params T[] array) {
			var ret = new List<T>(array.Length);
			foreach(var item in array)
				ret.Add(item);
			return ret;
		}
		
		
		
		// DICTIONARIES

		/**Returns the value with this key. If there is none, uses the creator to create a value and add it to the dictionary first.*/
		public static V GetOrCreate<K, V>(this Dictionary<K, V> map, K key, Func<V> valueCreator) {
			V value;
			if (!map.TryGetValue(key, out value)) {
				value = valueCreator.Invoke();
				map[key] = value;
			}
			return value;
		}

		/**Returns null if the item isn't found.*/
		public static V Opt<K, V>(this Dictionary<K, V> map, [NotNull] K key) where V : class {
			if (map.TryGetValue(key, out var value))
				return value;
			return null;
		}

		/**Throws an error if the key already exists.*/
		public static void PutNew<K, V>(this Dictionary<K, V> map, [NotNull] K key, [NotNull] V value) {
			if(map.TryGetValue(key, out var current))
				throw new Exception($"Dictionary already contains key {key}, with value {current}.");
			map[key] = value;
		}

		/**Throws an error if the key isn't found, and includes the key in the message (unlike the [] message).*/
		public static V Require<K, V>(this Dictionary<K, V> map, [NotNull] K key) {
			if (map.TryGetValue(key, out var value))
				return value;
			throw new Exception($"Key {key} not found.");
		}

		/**Generates a new random unsigned int that is not currently in the map, returns it, and stores the provided value under this key. TODO move to rand data gen*/
		public static uint PutRandKey<V>(this Dictionary<uint, V> map, V value) {
			for (var c = 0; c < 10; c++) {
				var key = ThreadLocalRandom.Current.NextUint(1);
				if (!map.ContainsKey(key)) {
					map[key] = value;
					return key;
				}
			}
			throw new Exception("Failed to find a unique uint to use as a key in a map.");
		}
		
		
		
		//MATH
		
		/**Both numbers should be positive. Returns difference / average.*/
		public static float RelativeDifference(float a, float b) => 2 * (b - a) / (a + b);
		
		/**Returns +1 if true, -1 if false.*/
		public static int Sign(bool positive) => positive ? 1 : -1;
		
		
		// RANDOM DATA GENERATION
		//TODO move to ThreadLocalRandom

		public static float NextFloat(this Random rand, double min, double max) => (float) (min + rand.NextDouble() * (max - min));

		/**Returns null if the list is empty, or a random entry otherwise.*/
		public static T NextInOpt<T>(this Random rand, IList<T> list) where T : class => list.Count == 0 ? null : list[rand.Next(list.Count)];

		public static uint NextUint(this Random rand) {
			var bytes = new byte[4];
			rand.NextBytes(bytes);
			return BitConverter.ToUInt32(bytes, 0);
		}

		/**The distribution isn't quite uniform, but it should be close enough.*/
		public static uint NextUint(this Random rand, uint min, uint max = UInt32.MaxValue) => min + rand.NextUint() % (max - min);
	}

}
