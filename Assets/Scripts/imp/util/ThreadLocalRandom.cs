﻿using System;


namespace imp {

	public class ThreadLocalRandom {
		private static readonly Random _global = new Random();
		[ThreadStatic] private static Random _local;

		public static Random Current {
			get {
				var ret = _local;
				if (ret != null)
					return ret;
				lock (_global) {
					ret = _local;
					if (ret != null)
						return ret;
					return _local = new Random(_global.Next());
				}
			}
		}
	}
}