using System;
using System.Collections.Generic;

namespace imp {
	/// An observable property you can register listeners on.
	/// When the value changes, the listeners are called, in the thread making the change (normally the Unity main thread).
	/// Change listeners are not called if the new value == the old one.
	public class ImpProp<T> {
		private T _value;
		private List<Action<T>> _postChange = new List<Action<T>>();

		public T Value {
			get => _value;
			set {
				if (_value.Equals(value))
					return;
				_value = value;
				Changed();
			}
		}

		public ImpProp(T initialValue) {
			_value = initialValue;
		}

		/// This function will be called every time the value changes. You can't remove a listener.
		public void PostChange(Action<T> listener) => _postChange.Add(listener);

		/// This function will be called every time the value changes, and also immediately. You can't remove a listener.
		public void PostChangeAndImmediate(Action<T> listener) {
			PostChange(listener);
			listener.Invoke(_value);
		}

		/// If you change something inside the value contained by this property, but don't assign a new value, call this to trigger post-change listeners.
		public void Changed() {
			foreach(var listener in _postChange)
				listener.Invoke(_value);
		}
	}
}