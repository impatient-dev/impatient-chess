﻿using System;
using imp.util;
using imp.chess.game;
using imp.chess.ui;
using imp.chess.ui.game;
using UnityEngine;


public class StarterScript : MonoBehaviour {
	
	void Start() {
		Debug.Log("Starting impatient-chess.");
		Application.logMessageReceived += LogCallback;
		Time.timeScale = 1;
		Application.targetFrameRate = 30;

		var setup = new BoardSetup();
		setup.White = Chess2.TwoKings;
		setup.Black = Chess2.Nemesis;
		var r = new AppRes();
		var state = new GameState(setup, Turn.White, setup.Create());
		AppMainScript.Create(new GameScene(state, r));
		state.Winner.Value = new GameWinner(true, WinReason.Checkmate); //TODO
		this.gameObject.Destroy();
	}


	private static void LogCallback(string condition, string stackTrace, LogType type) {
		if(type == LogType.Exception || type == LogType.Error || type == LogType.Assert) {
			Debug.LogWarning("Error encountered; shutting down.");
			Time.timeScale = 0;
			Application.Quit(1);
		}
	}
}